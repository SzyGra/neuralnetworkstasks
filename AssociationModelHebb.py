#Grad Szymon
#Cw 3
#Python

print('Wprowadzenie do teorii sieci neuronowych')
print('Cwiczenie nr 3')
print('#Model asocjacji')

def ClearVector(value):
	vector=[]

	for i in range(0,25):
 		vector[i]=value
	return vector
def PrepareZ0(type):
	z0=[]
	z0=[-1.0]*25
	if type=='normal':
		z0[6]=1.0
		z0[7] = 1.0
		z0[8] = 1.0
		z0[11] = 1.0
		z0[13] = 1.0
		z0[16] = 1.0
		z0[17] = 1.0
		z0[18] = 1.0
	elif type=='disturbed':
		z0[1] = 1.0
		z0[2] = 1.0
		z0[3] = 1.0
		z0[6] = 1.0
		z0[8] = 1.0
		z0[11] = 1.0
		z0[13] = 1.0
		z0[16] = 1.0
		z0[17] = 1.0
		z0[18] = 1.0
	return z0
def PrepareZ1(type):
	z1=[]
	z1=[-1.0]*25
	if type=='normal':
		z1[6] = 1.0
		z1[7] = 1.0
		z1[12] = 1.0
		z1[17] = 1.0
	elif type=='disturbed':
		z1[2] = 1.0
		z1[7] = 1.0
		z1[12] = 1.0
		z1[17] = 1.0
		z1[22] = 1.0
	return z1
def calculateW (z0,z1):
	W=[[0.0]*25 for i in range(0,25)]
	for i in range(0,25):
		for j in range(0,25):
			W[i][j]=1.0 / (25.0 * (z0[i] * z0[j] + z1[i] + z0[j]))
	return W
def calculateSGN(x):
	z=[0.0]*25
	for i in range(0,25):
	if x[i]>=0.0:
		z[i]=1.0
	else:
		z[i]=-1.0
	return z
def calculateF(W,u):
	y=[1.0]*25
	yValue=0.0
	for i in range (0,25):
		for j in range(0,25):
			yValue+=W[i][j] *u[j]
		y[i]=yValue
		yValue=0.0
	y=calculateSGN(y)
	return y
def displayResult(vector):
	for i in range(0,25):
		if vector[i]==-1.0:
			print(' [ ] ',end='')
		else:
			print(' [*] ',end='')
		if i%5 == 4:
			print()

z0=PrepareZ0('normal')
z0_=PrepareZ0('disturbed')

z1=PrepareZ1('normal')
z1_=PrepareZ1('disturbed')

w=calculateW(z0,z1)
y=[]

print(f'z0 ({z0})')
y=calculateF(w,z0)
displayResult(y)
print()
print('z0 - zaburzone')
displayResult(z0_)
print()
print(f'z0 - zaburzone (rezultat) {z0_}')
y=calculateF(w,z0_)
displayResult(y)
print()
print()
print(f'z1 ({z1})')
y=calculateF(w,z1)
displayResult(y)
print()
print('z1 - zaburzone')
displayResult(z1_)
print()
print(f'zaburzone (rezultat) {z1_}')
y=calculateF(w,z1_)
displayResult(y)
print()