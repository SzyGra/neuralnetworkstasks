#Grad Szymon
#Cw 4
#Python

c=0.01
epsilon=0.00000001
def calcFirstX(vec):
        return 4.0 * float(vec[0]) - 2.0 * float(vec[1]) -2.0
def calcFirstY(vec):
        return 4.0 * float(vec[1]) - 2.0 * float(vec[0]) -2.0 * float(vec[2])
def calcFirstZ(vec):
        return 2.0 * float(vec[2]) -2.0 * float(vec[1])
def calcSecondX(vec):
        return 12.0 * pow(float(vec[0]),3.0) + 12.0 * pow(float(vec[0]),2.0) - 24.0 * float(vec[0])
def calcSecondY (vec):
        return 24.0 * float(vec[1]) -24.0
def gradientFirst (input):
        vectorNew=[]
        flag = True
        max = 0.0
        for i in range(0,3):
                vectorNew.append(0.0)
        while (flag):
                vectorNew[0] = input[0] - c * calcFirstX(input)
                vectorNew[1] = input[1] - c * calcFirstY(input)
                vectorNew[2] = input[2] - c * calcFirstZ(input)
                max = 0.0
                for i in range(0,3):
                        if abs(vectorNew[i]-input[i])>max:
                                max =abs(vectorNew[i] - input[i])
                if max < epsilon:
                        print('punkt: {0}, {1}, {2}, \nwartosc: {3}'.format(vectorNew[0],vectorNew[1],vectorNew[2],2* pow(vectorNew[0],2.0)+ 2 * pow(vectorNew[1],2.0)+ vectorNew[2]*vectorNew[2] - 2* vectorNew[0]*vectorNew[1] - 2 * vectorNew[2]*vectorNew[1] - 2 * vectorNew[0] + 3))
                        flag=False
                input=vectorNew.copy()
def gradientSecond (input):
        vectorNew=[]
        flag= True
        max = 0.0
        for i in range(0,2):
                vectorNew.append(i)
        while(flag):
                vectorNew[0] = input[0] - c * calcSecondX(input)
                vectorNew[1] = input[1] - c * calcSecondY(input)

                max=0.0
                for i in range(0,2):
                        if abs(vectorNew[i] - input[i])>max:
                                max = abs(vectorNew[i] - input[i])
                if max<epsilon:
                        print('punkt: {0}, {1} \nwartosc: {2}'.format(vectorNew[0],vectorNew[1],3.0* pow(vectorNew[0],4) + 4.0* pow(vectorNew[0],3) -12.0 *pow(vectorNew[0],2)+12.0*pow(vectorNew[1],2) -24.0 * vectorNew[1]))
                        flag=False
                input=vectorNew.copy()
xt=[1.0,0.0,1.0]
print('First gradient method:')
gradientFirst(xt)
xt_new=[4.0,4.0]
print('Second gradient method:')
gradientSecond(xt_new)