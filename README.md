# Neural network tasks

Implementation of neural networks tasks in Python 3.5

## Tasks:

1.  McCulloch-Pitts neuron
2.  Minsky-Papert perceptron
3.  Association model - Hebb
4.  GradientMethod (X,Y,Z and X,Y) 